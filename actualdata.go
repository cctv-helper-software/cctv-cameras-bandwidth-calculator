package main

type actualData struct {
	Resolution  string
	Quality     string
	Compression string
	FPS         int
	Cameras     int
}

func NewActualData(defaults defaults, resolutionSwitch, qualitySwitch, compressionSwitch string, fpsSwitch, camerasSwitch int) actualData {
	actualData := actualData{}
	actualData.Resolution = defaults.Resolution
	if resolutionSwitch != "" {
		actualData.Resolution = resolutionSwitch
	}
	actualData.Quality = defaults.Quality
	if qualitySwitch != "" {
		actualData.Quality = qualitySwitch
	}
	actualData.Compression = defaults.Compression
	if compressionSwitch != "" {
		actualData.Compression = compressionSwitch
	}
	actualData.FPS = defaults.FPS
	if fpsSwitch > 0 {
		actualData.FPS = fpsSwitch
	}
	actualData.Cameras = defaults.Cameras
	if camerasSwitch > 0 {
		actualData.Cameras = camerasSwitch
	}

	return actualData
}
