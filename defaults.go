package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const defaultFileName = "defaults.txt"

type defaults struct {
	Resolution  string
	Quality     string
	Compression string
	FPS         int
	Cameras     int
}

func NewDefaults(resolution resolution, quality quality, compression compression) defaults {
	defaults := defaults{}
	readDefaults(&defaults, resolution, quality, compression)
	testDefaults(&defaults)
	return defaults
}

func readDefaults(defaults *defaults, resolution resolution, quality quality, compression compression) {
	inputFile, err := os.Open(defaultFileName)
	if err != nil {
		fmt.Println("Error opening input file:", err)
	}
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "Resolution = ") {
			for _, v := range resolution.Names {
				if strings.HasSuffix(scanner.Text(), "\""+v+"\"") {
					defaults.setResolution(v)
				}
			}
		} else if strings.HasPrefix(scanner.Text(), "Quality") {
			for _, v := range quality.Names {
				if strings.HasSuffix(scanner.Text(), "\""+v+"\"") {
					defaults.setQuality(v)
				}
			}
		} else if strings.HasPrefix(scanner.Text(), "Compression") {
			for _, v := range compression.Names {
				if strings.HasSuffix(scanner.Text(), "\""+v+"\"") {
					defaults.setCompression(v)
				}
			}
		} else if strings.HasPrefix(scanner.Text(), "FPS") {
			fpsreplacer := strings.NewReplacer(" ", "", "=", "", "FPS", "")
			defaults.setFPS(fpsreplacer.Replace(scanner.Text()))
		} else if strings.HasPrefix(scanner.Text(), "Cameras") {
			camerasreplacer := strings.NewReplacer(" ", "", "=", "", "Cameras", "")
			defaults.setCameras(camerasreplacer.Replace(scanner.Text()))
		}
	}
}

func testDefaults(defaults *defaults) {
	if defaults.Resolution == "" {
		defaults.Resolution = "2 Mpix (1920x1080 px)"
	}
	if defaults.Quality == "" {
		defaults.Quality = "Medium"
	}
	if defaults.Compression == "" {
		defaults.Compression = "h.264"
	}
	if defaults.FPS <= 0 {
		defaults.FPS = 15
	}
	if defaults.Cameras <= 0 {
		defaults.Cameras = 1
	}
}

func (d *defaults) setResolution(resolution string) {
	d.Resolution = resolution
}

func (d *defaults) setQuality(quality string) {
	d.Quality = quality
}

func (d *defaults) setCompression(compression string) {
	d.Compression = compression
}

func (d *defaults) setFPS(fps string) {
	d.FPS, _ = strconv.Atoi(fps)
}

func (d *defaults) setCameras(cameras string) {
	d.Cameras, _ = strconv.Atoi(cameras)
}
