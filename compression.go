package main

type compression struct {
	NameMJPEG    string
	NameMPEG2    string
	NameMPEG4    string
	NameH264     string
	NameH265     string
	NameH265Plus string
	Names        []string
	NamesShort   map[string]string
	Factor       map[string]float64
}

func NewCompression() compression {
	compression := compression{}
	compression.NameMJPEG = "MJPEG"
	compression.NameMPEG2 = "MPEG2"
	compression.NameMPEG4 = "MPEG4"
	compression.NameH264 = "h.264"
	compression.NameH265 = "h.265"
	compression.NameH265Plus = "h.265+"
	compression.Names = []string{
		compression.NameMJPEG,
		compression.NameMPEG2,
		compression.NameMPEG4,
		compression.NameH264,
		compression.NameH265,
		compression.NameH265Plus,
	}
	compression.NamesShort = map[string]string{
		compression.NameMJPEG: "",
		compression.NameMPEG2: "",
		compression.NameMPEG4: "",
		"h264":                "",
		"h265":                "",
		"h265p":               "",
	}
	compression.Factor = map[string]float64{
		compression.NameMJPEG:    1.8,
		compression.NameMPEG2:    1.5,
		compression.NameMPEG4:    1.4,
		compression.NameH264:     1.0,
		"h264":                   1.0,
		compression.NameH265:     0.5,
		"h265":                   0.5,
		compression.NameH265Plus: 0.2,
		"h265p":                  0.2,
	}

	return compression
}
