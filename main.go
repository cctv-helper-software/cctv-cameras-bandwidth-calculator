package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"strconv"
)

func main() {
	resolutionFlag := flag.String("resolution", "", "QCIF, CIF, 4CIF, D1, 1Mpix, 2Mpix, 3Mpix, 4Mpix, 5Mpix, 6Mpix, 8Mpix, 12Mpix, 16Mpix")
	resolutionFlagShort := flag.String("r", "", "QCIF, CIF, 4CIF, D1, 1Mpix, 2Mpix, 3Mpix, 4Mpix, 5Mpix, 6Mpix, 8Mpix, 12Mpix, 16Mpix")
	qualityFlag := flag.String("quality", "", "Low, Medium, High")
	qualityFlagShort := flag.String("q", "", "Low, Medium, High")
	compressionFlag := flag.String("compression", "", "MJPEG, MPEG2, MPEG4, h264, h265, h265p")
	compressionFlagShort := flag.String("c", "", "MJPEG, MPEG2, MPEG4, h264, h265, h265p")
	fpsFlag := flag.Int("fps", 0, "frames per second, as positive integer")
	fpsFlagShort := flag.Int("f", 0, "frames per second, as positive integer")
	camerasFlag := flag.Int("cameras", 0, "amount of cameras, as positive integer")
	camerasFlagShort := flag.Int("m", 0, "amount of cameras, as positive integer")
	flagVerbose := flag.Bool("verbose", false, "show additional information")
	flagVerboseShort := flag.Bool("v", false, "show additional information")
	flag.Parse()

	resolution := NewResolution()
	quality := NewQuality()
	compression := NewCompression()
	defaults := NewDefaults(resolution, quality, compression)

	resolutionTmp := resolutionFlag
	if *resolutionTmp == "" {
		resolutionTmp = resolutionFlagShort
	}
	if _, ok := resolution.NamesShort[*resolutionTmp]; !ok {
		*resolutionTmp = ""
	}
	qualityTmp := qualityFlag
	if *qualityTmp == "" {
		qualityTmp = qualityFlagShort
	}
	if _, ok := quality.NamesShort[*qualityTmp]; !ok {
		*qualityTmp = ""
	}
	compressionTmp := compressionFlag
	if *compressionTmp == "" {
		compressionTmp = compressionFlagShort
	}
	if _, ok := compression.NamesShort[*compressionTmp]; !ok {
		*compressionTmp = ""
	}
	fpsTmp := fpsFlag
	if *fpsTmp == 0 {
		fpsTmp = fpsFlagShort
	}
	camerasTmp := camerasFlag
	if *camerasTmp == 0 {
		camerasTmp = camerasFlagShort
	}

	actualData := NewActualData(defaults, *resolutionTmp, *qualityTmp, *compressionTmp, *fpsTmp, *camerasTmp)

	fs := resolution.FrameSize[actualData.Resolution]
	qf := quality.Factor[actualData.Quality]
	cf := compression.Factor[actualData.Compression]
	fps := actualData.FPS
	camno := actualData.Cameras
	if *flagVerbose || *flagVerboseShort {
		fmt.Println()
		fmt.Println("Resolution:   ", actualData.Resolution)
		fmt.Println("Frame size:   ", fs)
		fmt.Println("Quality:      ", actualData.Quality)
		fmt.Println("Compression:  ", actualData.Compression)
		fmt.Println("FPS:          ", actualData.FPS)
		fmt.Println("Cameras:      ", actualData.Cameras)
	}
	bitrate := fs * qf * cf * float64(fps) / 100.0
	totalBitrate := bitrate * float64(camno)
	fmt.Println("\nTotal bitrate:", totalBitrate)
	var data = [][]string{
		{"Bitrate", "Cameras number", "Total bitrate"},
		{strconv.FormatFloat(bitrate, 'f', 2, 64), strconv.Itoa(camno), strconv.FormatFloat(totalBitrate, 'f', 2, 64)},
	}
	f, err := os.Create("finished_cameras_bandwidth_calculator.csv")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	writer := csv.NewWriter(f)
	defer writer.Flush()
	for _, v := range data {
		if err := writer.Write(v); err != nil {
			fmt.Println(err)
			return
		}
	}
}
