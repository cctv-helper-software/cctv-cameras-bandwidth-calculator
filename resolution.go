package main

type resolution struct {
	NameQCIF   string
	NameCIF    string
	Name4CIF   string
	NameD1     string
	Name1Mpix  string
	Name2Mpix  string
	Name3Mpix  string
	Name4Mpix  string
	Name5Mpix  string
	Name6Mpix  string
	Name8Mpix  string
	Name12Mpix string
	Name16Mpix string
	Names      []string
	NamesShort map[string]string
	FrameSize  map[string]float64
}

func NewResolution() resolution {
	resolution := resolution{}
	resolution.NameQCIF = "QCIF (176x120 px)"
	resolution.NameCIF = "CIF (352x240 px)"
	resolution.Name4CIF = "4CIF (704x480 px)"
	resolution.NameD1 = "D1 (720x480 px)"
	resolution.Name1Mpix = "1 Mpix (1280x720 px)"
	resolution.Name2Mpix = "2 Mpix (1920x1080 px)"
	resolution.Name3Mpix = "3 Mpix (2048x1536 px)"
	resolution.Name4Mpix = "4 Mpix (2560x1440 px)"
	resolution.Name5Mpix = "5 Mpix (2592x1944 px)"
	resolution.Name6Mpix = "6 Mpix (3072x2048 px)"
	resolution.Name8Mpix = "8 Mpix (3840x2160 px)"
	resolution.Name12Mpix = "12 Mpix (4000x3000 px)"
	resolution.Name16Mpix = "16 Mpix (4592x3056 px)"
	resolution.Names = []string{
		resolution.NameQCIF,
		resolution.NameCIF,
		resolution.Name4CIF,
		resolution.NameD1,
		resolution.Name1Mpix,
		resolution.Name2Mpix,
		resolution.Name3Mpix,
		resolution.Name4Mpix,
		resolution.Name5Mpix,
		resolution.Name6Mpix,
		resolution.Name8Mpix,
		resolution.Name12Mpix,
		resolution.Name16Mpix,
	}
	resolution.NamesShort = map[string]string{
		"QCIF":   "",
		"CIF":    "",
		"4CIF":   "",
		"D1":     "",
		"1Mpix":  "",
		"2Mpix":  "",
		"3Mpix":  "",
		"4Mpix":  "",
		"5Mpix":  "",
		"6Mpix":  "",
		"8Mpix":  "",
		"12Mpix": "",
		"16Mpix": "",
	}
	resolution.FrameSize = map[string]float64{
		resolution.NameQCIF:   0.4,
		"QCIF":                0.4,
		resolution.NameCIF:    1.0,
		"CIF":                 1.0,
		resolution.Name4CIF:   2.9,
		"4CIF":                2.9,
		resolution.NameD1:     3.3,
		"D1":                  3.3,
		resolution.Name1Mpix:  9.3,
		"1Mpix":               9.3,
		resolution.Name2Mpix:  13.8,
		"2Mpix":               13.8,
		resolution.Name3Mpix:  22.6,
		"3Mpix":               22.6,
		resolution.Name4Mpix:  24.8,
		"4Mpix":               24.8,
		resolution.Name5Mpix:  32.2,
		"5Mpix":               32.2,
		resolution.Name6Mpix:  40.0,
		"6Mpix":               40.0,
		resolution.Name8Mpix:  65.3,
		"8Mpix":               65.3,
		resolution.Name12Mpix: 101.5,
		"12Mpix":              101.5,
		resolution.Name16Mpix: 165.0,
		"16Mpix":              165.0,
	}

	return resolution
}
