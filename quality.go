package main

type quality struct {
	NameLow    string
	NameMedium string
	NameHigh   string
	Names      []string
	NamesShort map[string]string
	Factor     map[string]float64
}

func NewQuality() quality {
	quality := quality{}
	quality.NameLow = "Low"
	quality.NameMedium = "Medium"
	quality.NameHigh = "High"
	quality.Names = []string{
		quality.NameLow,
		quality.NameMedium,
		quality.NameHigh,
	}
	quality.NamesShort = map[string]string{
		quality.NameLow:    "",
		quality.NameMedium: "",
		quality.NameHigh:   "",
	}
	quality.Factor = map[string]float64{
		quality.NameLow:    0.5,
		quality.NameMedium: 1.0,
		quality.NameHigh:   1.5,
	}

	return quality
}
